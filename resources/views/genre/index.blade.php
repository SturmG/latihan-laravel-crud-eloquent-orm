@extends('layout.master')

@section('Judul')
Halaman List Genre
@endsection 

@section('content')

<a href="/genre/create" class="btn btn-success mb-3">Tambah data</a> 

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>   
                    <form action="/genre/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>    
                </td>
            </tr>
        @empty
        <tr>
            <td>Data kosong</td>   
        </tr>    
        @endforelse
    </tbody>
</table>

@endsection   