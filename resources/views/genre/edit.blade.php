@extends('layout.master')

@section('Judul')
Halaman Edit  {{$genre->nama}}     
@endsection

@section('content')

<form action="/genre/{{$genre->id}}" method="POST">
    @csrf  
    @method('PUT')
    <div class="form-group">
        <label>Nama Genre</label>
        <input type="text" name="nama" class="form-control" value="{{$genre->nama}}">
    </div>
    @error('nama') 
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Update</button>
    </form>


@endsection 
@section('content')