@extends('layout.master')

@section('Judul')
Halaman Edit Film     
@endsection

@section('content')

<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf  
    @method('PUT')
    <div class="form-group">
        <label>Judul Film</label>
        <input type="text" value="{{$film->judul}}" name="judul" class="form-control">
    </div>
    @error('judul') 
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
    </div>
    @error('ringkasan') 
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Tahun</label>
        <input type="text" name="tahun" class="form-control">
    </div>
    @error('tahun') 
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Poster</label>
        <input type="file" name="poster" class="form-control">
    </div>
    @error('poster') 
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>genre</label>
        <select name="genre_id" class="form-control" id="">
            <option value="">---Pilih Genre---</option>
            @foreach ($genre as $item)
                @if ($item->id === $film->genre_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>   
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
    </div>
    @error('genre_id') 
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Update</button>
    </form>


@endsection 