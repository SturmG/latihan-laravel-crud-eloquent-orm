@extends('layout.master')

@section('Judul')
Halaman Detail Film {{$film->judul}}     
@endsection

@section('content')

<img src="{{asset('image/'.$film->poster)}}" alt="">
<h1>{{$film->judul}}</h1>
<h4>{{$film->tahun}}</h4>
<p>{{$film->ringkasan}}</p>

<a href="/film" class="btn btn-secondary">Kembali</a>


@endsection
