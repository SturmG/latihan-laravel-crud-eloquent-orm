@extends('layout.master')

@section('Judul')
Halaman Film     
@endsection

@section('content')

<a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>

<div class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('image/'. $item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                <h1>{{$item->judul}}</h1>
                <p class="card-text">{{Str::limit($item->ringkasan, 30)}}</p>                
                <form action="/film/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
                </div>
            </div>
        </div>
    @empty
        <h4>Data film kosong</h4>
    @endforelse
    
</div>

@endsection