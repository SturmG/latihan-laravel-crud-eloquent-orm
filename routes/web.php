<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/form', 'AuthController@form');

Route::post('/welcome', 'AuthController@signup');

Route::get('/data-table', function(){
    return view('Table.data-table');
});

Route::get('/table', function(){
    return view('Table.table');
});

// CRUD Genre
Route::get('/genre/create', 'GenreController@create');
Route::post('/genre', 'GenreController@store');
Route::get('/genre', 'GenreController@index');
Route::get('/genre/{genre}', 'GenreController@show'); 
Route::get('/genre/{genre}/edit', 'GenreController@edit');
Route::put('/genre/{genre}', 'GenreController@update');
Route::delete('/genre/{genre}', 'GenreController@destroy'); 

//CRUD Film
Route::resource('film', 'FilmController');