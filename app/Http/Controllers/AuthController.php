<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('Halaman.form');
    }

    public function signup(Request $request){
        $name1 = $request['firstname'];
        $name2 = $request['lastname'];
        return view('Halaman.welcome', compact('name1', 'name2'));
    }
}
